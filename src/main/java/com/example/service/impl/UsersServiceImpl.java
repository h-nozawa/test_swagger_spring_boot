package com.example.service.impl;

import com.example.Repository.UsersRepository;
import com.example.User;
import com.example.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository repository;

    @Override
    public List<User> getById(int id) {
        return repository.findById(id);
    }


}

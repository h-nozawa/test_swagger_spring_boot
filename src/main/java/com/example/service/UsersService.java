package com.example.service;

import com.example.User;

import java.util.List;

public interface UsersService {

    List<User> getById(int id);

}

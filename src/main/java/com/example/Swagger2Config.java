package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.google.common.base.Predicate;

import static springfox.documentation.builders.PathSelectors.*;
import static com.google.common.base.Predicates.*;

/**
 * Created by hiromi nozawa on 18/04/11.
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket swaggerSpringMvcPlugin() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(paths())
                .build()
                .apiInfo(apiInfo());
    }

    private Predicate<String> paths() {

        // 仕様書生成対象のURLパスを指定する
        return or(
                regex("/users.*"));

    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "System Web API",              // title
                "System の Web API 仕様書",    // description
                "0.0.1",                                // version
                "",                                     // terms of service url
                "hiromi nozawa",                             // created by
                "creationline,inc",                     // license
                "");                                    // license url
        return apiInfo;
    }
}
